<?php
/*
  Plugin Name: Custom Registration Login
  Description: Custom plugin for users login/registration
  Version: 1.0
  Author: Pavlo Salata
 */
ob_start();

function registration_form() {
    echo '
    <style>
    div {
      margin-bottom:2px;
    }
     
    input{
        margin-bottom:4px;
    }
    </style>
    ';

    echo '
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
    <div>
    <label for="username">Username <strong>*</strong></label>
    <input type="text" name="username" value="userName">
    </div>
     
    <div>
    <label for="password">Password <strong>*</strong></label>
    <input type="password" name="password" value="password">
    </div>
     
    <div>
    <label for="email">Email <strong>*</strong></label>
    <input type="text" name="email" value="Email@gmail.com">
    </div>
     
     
  
    <input type="submit" name="submit" value="Register"/>
    </form>
    ';
}

function registration_validation( $username, $password, $email )  {
    global $reg_errors;
    $reg_errors = new WP_Error;
    if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
        $reg_errors->add('field', 'Не заповнені обов\язкові поля');
    }
    if ( 4 > strlen( $username ) ) {
        $reg_errors->add( 'username_length', 'Ім\'я користувача повинно бути не менше 4 символів' );
    }
    if ( username_exists( $username ) )
        $reg_errors->add('user_name', 'Таке ім\'я користувача вже існує!');
    if ( ! validate_username( $username ) ) {
        $reg_errors->add( 'username_invalid', 'Не може бути використаним так ім\'я користувача' );
    }
    if ( 5 > strlen( $password ) ) {
        $reg_errors->add( 'password', 'Довжина паролю повинна бути більше 5 символів' );
    }
    if ( !is_email( $email ) ) {
        $reg_errors->add( 'email_invalid', 'Не існуюча почта' );
    }
    if ( email_exists( $email ) ) {
        $reg_errors->add( 'email', 'Почта вже використовується' );
    }
    if ( is_wp_error( $reg_errors ) ) {

        foreach ( $reg_errors->get_error_messages() as $error ) {

            echo '<div>';
            echo '<strong>ERROR</strong>:';
            echo $error . '<br/>';
            echo '</div>';

        }
    }
}

function complete_registration() {
    global $reg_errors, $username, $password, $email;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        wp_create_user( $username,$password, $email );
    }

}

function custom_registration_function() {
    if ( isset($_POST['submit'] ) ) {
        registration_validation(
            $_POST['username'],
            $_POST['password'],
            $_POST['email']
        );

        global $username, $password, $email;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );


        complete_registration();

    }
    registration_form();

}

add_shortcode( 'cr_custom_registration', 'custom_registration_shortcode' );


function custom_registration_shortcode() {

    custom_registration_function();
    return ob_get_clean();
}