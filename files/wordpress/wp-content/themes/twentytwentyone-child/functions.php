<?php
/**
 * Настройка автологина НАЧАЛО
 *
 */
function auto_login_new_user( $user_id ) {
    wp_set_current_user($user_id);
    wp_set_auth_cookie($user_id);
    $user = get_user_by( 'id', $user_id );
    do_action( 'wp_login', $user->user_login );
    wp_redirect( home_url() );
    exit;
}

add_action( 'user_register', 'auto_login_new_user' );

/**
 * Настройка автологина КОНЕЦ
 *
 */

use PHPMailer\PHPMailer\PHPMailer;

/**
 * Настройка SMTP НАЧАЛО
 *
 * @param PHPMailer $phpmailer объект мэилера
 */
function send_smtp_email( PHPMailer $phpmailer ) {
    $phpmailer->isSMTP();
    $phpmailer->Host       = SMTP_HOST;
    $phpmailer->SMTPAuth   = SMTP_AUTH;
    $phpmailer->Port       = SMTP_PORT;
    $phpmailer->Username   = SMTP_USER;
    $phpmailer->Password   = SMTP_PASS;
    $phpmailer->SMTPSecure = SMTP_SECURE;
    $phpmailer->From       = SMTP_FROM;
    $phpmailer->FromName   = SMTP_NAME;
}
add_action( 'phpmailer_init', 'send_smtp_email' );
/**
 * Настройка SMTP КОНЕЦ
 *
 */

/**
 *  Поменять почту с автора статтьи на админа НАЧАЛО
 *
 */

function se_comment_moderation_recipients( $emails, $comment_id ) {
    $comment = get_comment( $comment_id );
    $post = get_post( $comment->comment_post_ID );
    $user = get_user_by( 'id', '1' );

    if ( user_can( $user->ID, 'edit_published_posts' ) && ! empty( $user->user_email ) ) {
        $emails = array( $user->user_email );
    }

    return $emails;
}
add_filter( 'comment_moderation_recipients', 'se_comment_moderation_recipients', 11, 2 );
add_filter( 'comment_notification_recipients', 'se_comment_moderation_recipients', 11, 2 );

/**
 *  Поменять почту с автора статтьи на админа КОНЕЦ
 *
 */