<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();
if ( is_user_logged_in() ) {
    if ( have_posts() ) {

        // Load posts loop.
        while ( have_posts() ) {
            the_post();

            get_template_part( 'template-parts/content/content', get_theme_mod( 'display_excerpt_or_full_post', 'excerpt' ) );
        }

        // Previous/next page navigation.
        twenty_twenty_one_the_posts_navigation();

    } else {

        // If no content, include the "No posts found" template.
        get_template_part( 'template-parts/content/content-none' );

    }
} else {
    echo '<a href="http://94.237.98.122/%d0%bb%d0%be%d0%b3%d1%96%d0%bd-%d1%80%d0%b5%d1%94%d1%81%d1%82%d1%80%d0%b0%d1%86%d1%96%d1%8f/">Зареєструйтесь або увійдіть</a>';
}


get_footer();
